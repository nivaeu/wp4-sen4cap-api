#
# Copyright (c) Sinergise, 2019 -- 2021.
#
# This file belongs to common component "Sen4CAP component API" of project NIVA (www.niva4cap.eu).
# All rights reserved.
#
# This source code is licensed under the Apache License v2 license found in the LICENSE
# file in the root directory of this source tree.
#

import pytest
import sys, os
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import process
from process._common import DataCube, DimensionType, assert_equal


@pytest.fixture
def generate_data():
    def _construct(
        data=([[[[0.2, 0.8]]]], [[[[0.2, 0.8]]]]),
        attrs={"test_keep_attrs": 42},
        dims=("t", "y", "x", "band"),
        dim_types={
            "x": DimensionType.SPATIAL,
            "y": DimensionType.SPATIAL,
            "t": DimensionType.TEMPORAL,
            "band": DimensionType.BANDS,
        },
        as_list=False,
        as_dataarray=False,
    ):
        if as_list:
            return data

        if as_dataarray:
            return DataCube(data, dims=dims, attrs=attrs, dim_types=dim_types)

        data_list = []

        for d in data:
            xrdata = DataCube(d, dims=dims, attrs=attrs, dim_types=dim_types)
            data_list.append(xrdata)

        return data_list

    return _construct


@pytest.fixture
def execute_product_process(generate_data):
    def wrapped(data_arguments={}, ignore_nodata=None):
        arguments = {}
        if data_arguments is not None:
            arguments["data"] = generate_data(**data_arguments)
        if ignore_nodata is not None:
            arguments["ignore_nodata"] = ignore_nodata

        return process.product.productEOTask(None, "", None, {}, "node1", {}).process(arguments)

    return wrapped


###################################
# tests:
###################################


@pytest.mark.parametrize(
    "data,ignore_nodata,expected_result", [([5, 0], True, 0), ([-2, 4, 2.5], True, -20), ([1, None], False, None)]
)
def test_examples(execute_product_process, data, expected_result, ignore_nodata):
    """
    Test product process with examples from https://open-eo.github.io/openeo-api/processreference/#product
    """
    data_arguments = {"data": data, "as_list": True}
    result = execute_product_process(data_arguments, ignore_nodata=ignore_nodata)
    assert result == expected_result


@pytest.mark.parametrize(
    "array1,array2,expected_data",
    [
        ([[[[0.2, 0.8]]]], [[[[0.2, 0.8]]]], [[[[0.04, 0.64]]]]),
    ],
)
def test_with_xarray(execute_product_process, generate_data, array1, array2, expected_data):
    """
    Test product process with xarray.DataArrays
    """
    expected_result = generate_data(data=[expected_data])[0]
    result = execute_product_process({"data": (array1, array2)})
    assert_equal(result, expected_result)


@pytest.mark.parametrize(
    "array1,array2,ignore_nodata,expected_data",
    [
        ([[[[np.nan, 0.0]]]], [[[[0.2, np.nan]]]], True, [[[[0.2, 0.0]]]]),
        ([[[[np.nan, np.nan]]]], [[[[0.2, np.nan]]]], False, [[[[np.nan, np.nan]]]]),
        ([[[[2.0, 1.0]]]], [[[[0.2, np.nan]]]], False, [[[[0.4, np.nan]]]]),
    ],
)
def test_with_xarray_nulls(execute_product_process, generate_data, array1, array2, expected_data, ignore_nodata):
    """
    Test product process with xarray.DataArrays with null in data
    """
    expected_result = generate_data(data=[expected_data])[0]
    result = execute_product_process({"data": (array1, array2)}, ignore_nodata=ignore_nodata)
    assert_equal(result, expected_result)


@pytest.mark.parametrize(
    "array1,array2,expected_data",
    [
        ([[[[0.2, 0.8]]]], [[[[0.2, 0.8]]]], [[[[0.04, 0.64]]]]),
    ],
)
def test_product(generate_data, array1, array2, expected_data):
    """
    Test product process, which is an alias of product
    """
    expected_result = generate_data(data=[expected_data])[0]
    arguments = {"data": generate_data(data=[array1, array2])}
    result = process.product.productEOTask(None, "", None, {}, "node1", {}).process(arguments)
    assert_equal(result, expected_result)


@pytest.mark.parametrize(
    "data,reduce_by,expected_data,expected_dims",
    [
        ([[[[0.2, 0.8]]], [[[1.0, 2.0]]]], "t", [[[0.2, 1.6]]], ("y", "x", "band")),
    ],
)
def test_xarray_directly(execute_product_process, generate_data, data, reduce_by, expected_data, expected_dims):
    """
    Test product process by passing a DataArray to be reduced directly (instead of a list)
    """
    expected_result = generate_data(
        data=expected_data, dims=expected_dims, attrs={"reduce_by": reduce_by}, as_dataarray=True
    )
    result = execute_product_process({"data": data, "attrs": {"reduce_by": reduce_by}, "as_dataarray": True})
    assert_equal(result, expected_result)


@pytest.mark.parametrize(
    "number1,number2,arr1,arr2,expected_data,num_first",
    [
        (
            1000,
            0.1,
            [[[[1.0, 2.0]]], [[[4.0, 5.0]]]],
            [[[[20.0, 10.0]]], [[[5.0, 4.0]]]],
            [[[[2000, 2000]]], [[[2000, 2000]]]],
            True,
        ),
        (
            1000,
            0.1,
            [[[[1.0, 2.0]]], [[[4.0, 5.0]]]],
            [[[[20.0, 10.0]]], [[[5.0, 4.0]]]],
            [[[[2000, 2000]]], [[[2000, 2000]]]],
            False,
        ),
    ],
)
def test_with_numbers(execute_product_process, generate_data, number1, number2, arr1, arr2, expected_data, num_first):
    """
    Test product process by a list of numbers and DataArrays
    """
    expected_result = generate_data(data=expected_data, as_dataarray=True)
    arr1 = generate_data(data=arr1, as_dataarray=True)
    arr2 = generate_data(data=arr2, as_dataarray=True)

    if num_first:
        data = [number1, arr1, number2, arr2]
    else:
        data = [arr1, number1, arr2, number2]

    result = execute_product_process({"data": data, "as_list": True})
    assert_equal(result, expected_result)
