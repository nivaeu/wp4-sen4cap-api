#
# Copyright (c) Sinergise, 2019 -- 2021.
#
# This file belongs to common component "Sen4CAP component API" of project NIVA (www.niva4cap.eu).
# All rights reserved.
#
# This source code is licensed under the Apache License v2 license found in the LICENSE
# file in the root directory of this source tree.
#

import datetime
import os
import sys
import json

from botocore.stub import Stubber, ANY
from io import BufferedReader
import pytest
from sentinelhub import BBox, CRS

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import process
from process._common import ProcessParameterInvalid, ProcessArgumentRequired, Band, DataCube, DimensionType


FIXTURES_FOLDER = os.path.join(os.path.dirname(__file__), "fixtures")

S3_BUCKET_NAME = "com.sinergise.openeo.results"
DATA_AWS_S3_ENDPOINT_URL = os.environ.get("DATA_AWS_S3_ENDPOINT_URL")
JOB_ID = "random_job_id"


@pytest.fixture
def save_resultEOTask():
    return process.save_result.save_resultEOTask(None, JOB_ID, None, {}, "node1", {})


@pytest.fixture
def create_result_object():
    class CustomEqualityObject(BufferedReader):
        def __init__(self, parent):
            self.content = parent.read()

        def __eq__(self, other):
            other_content = other.read()
            return self.content == other_content

    def _generate(file):
        filename = os.path.join(FIXTURES_FOLDER, file)
        obj = open(filename, "rb")
        return CustomEqualityObject(obj)

    return _generate


@pytest.fixture
def generate_data():
    def _construct(
        xmin=12.32271,
        xmax=12.33572,
        ymin=42.06347,
        ymax=42.07112,
        data=[[[[0.2]]]],
        dims=("t", "y", "x", "band"),
        coords={"band": ["ndvi"], "t": [datetime.datetime.now()]},
        attrs={},
        dim_types={"band": DimensionType.BANDS},
    ):
        fake_bbox = BBox([xmin, ymin, xmax, ymax], CRS("4326"))
        attrs = {"bbox": fake_bbox, **attrs}

        if "band" in coords:
            coords["band"] = [Band(b) for b in coords["band"]]

        xrdata = DataCube(data, dims=dims, coords=coords, attrs=attrs, dim_types=dim_types)

        return xrdata

    return _construct


@pytest.fixture
def execute_save_result_process(generate_data, save_resultEOTask):
    def wrapped(data_arguments={}, file_format="GTiff", options=None):
        arguments = {}
        if data_arguments is not None:
            arguments["data"] = generate_data(**data_arguments)
        if file_format is not None:
            arguments["format"] = file_format
        if options is not None:
            arguments["options"] = options

        return save_resultEOTask.process(arguments)

    return wrapped


@pytest.fixture
def s3_stub_generator(save_resultEOTask):
    client = save_resultEOTask._s3
    with Stubber(client) as stubber:

        def wrapper(stubber):
            def _set_params(body, bucket=S3_BUCKET_NAME, stubber=stubber):
                stubber.add_response(
                    "put_object",
                    expected_params={
                        "ACL": ANY,
                        "Body": body,
                        "Bucket": bucket,
                        "ContentType": ANY,
                        "Expires": ANY,
                        "Key": ANY,
                    },
                    service_response={},
                )
                return stubber

            return _set_params

        set_params = wrapper(stubber)

        yield set_params


###################################
# tests:
###################################


@pytest.mark.parametrize(
    "parameters,expected_result_filename",
    [
        ({"file_format": "GTiff", "options": {"datatype": "float64"}}, "save_result_s3_file.tiff"),
        (
            {
                "file_format": "gtiff",
                "options": {"datatype": "byte"},
                "data_arguments": {
                    "data": [[[[0, 127, 255]]]],
                    "coords": {"band": ["r", "g", "b"], "t": [datetime.datetime.now()]},
                },
            },
            "save_result_s3_file_byte.tiff",
        ),
        (
            {
                "file_format": "png",
                "options": {"datatype": "byte"},
                "data_arguments": {
                    "data": [[[[0, 127, 255]]]],
                    "coords": {"band": ["r", "g", "b"], "t": [datetime.datetime.now()]},
                },
            },
            "save_result_s3_file.png",
        ),
        (
            {
                "file_format": "jpeg",
                "options": {"datatype": "byte"},
                "data_arguments": {
                    "data": [[[[255, 127, 0]]]],
                    "coords": {"band": ["r", "g", "b"], "t": [datetime.datetime.now()]},
                },
            },
            "save_result_s3_file.jpeg",
        ),
    ],
)
def test_correct(
    execute_save_result_process, s3_stub_generator, create_result_object, parameters, expected_result_filename
):
    """
    Test save_result process with correct parameters
    """
    s3_stub = s3_stub_generator(create_result_object(expected_result_filename))
    result = execute_save_result_process(**parameters)
    s3_stub.assert_no_pending_responses()
    assert result is True


@pytest.mark.parametrize(
    "parameters,filename",
    [
        (
            {
                "file_format": "json",
                "data_arguments": {
                    "data": [[[[255, 127, 0]]]],
                    "coords": {"band": ["r", "g", "b"], "t": [datetime.datetime(2020, 11, 10, 13, 46, 15)]},
                },
            },
            "save_result_s3_file.json",
        ),
    ],
)
def test_json(execute_save_result_process, s3_stub_generator, parameters, filename):
    """
    Test save_result process with correct parameters
    """
    with open(os.path.join(FIXTURES_FOLDER, filename)) as f:
        expected_json = f.read()
    s3_stub = s3_stub_generator(expected_json)
    result = execute_save_result_process(**parameters)
    s3_stub.assert_no_pending_responses()
    assert result is True


@pytest.mark.parametrize(
    "missing_required_parameter,failure_reason",
    [
        ({"data_arguments": None}, "data"),
        ({"file_format": None}, "format"),
    ],
)
def test_required_params(execute_save_result_process, missing_required_parameter, failure_reason):
    """
    Test save_result process with missing required parameters
    """
    with pytest.raises(ProcessArgumentRequired) as ex:
        result = execute_save_result_process(**missing_required_parameter)

    assert ex.value.args[0] == "Process 'save_result' requires argument '{}'.".format(failure_reason)


@pytest.mark.parametrize(
    "invalid_parameter,failure_param,failure_reason",
    [
        ({"file_format": "xcf"}, "format", "Supported formats are: gtiff, png, jpeg, json."),
        (
            {"options": {"option_name": "option_value"}},
            "options",
            "Supported options are: 'datatype'.",
        ),
    ],
)
def test_invalid_params(execute_save_result_process, invalid_parameter, failure_param, failure_reason):
    """
    Test save_result process with invalid parameters
    """
    with pytest.raises(ProcessParameterInvalid) as ex:
        result = execute_save_result_process(**invalid_parameter)
    assert ex.value.args == ("save_result", failure_param, failure_reason)
