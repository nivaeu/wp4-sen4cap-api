#
# Copyright (c) Sinergise, 2019 -- 2021.
#
# This file belongs to common component "Sen4CAP component API" of project NIVA (www.niva4cap.eu).
# All rights reserved.
#
# This source code is licensed under the Apache License v2 license found in the LICENSE
# file in the root directory of this source tree.
#

import numpy as np
import math

from ._common import ProcessEOTask, ProcessParameterInvalid, DataCube


class linear_scale_rangeEOTask(ProcessEOTask):
    """
    This process is often used within apply process. Apply could pass each of the values separately,
    but this would be very inefficient. Instead, we get passed a whole xarray, which is the reason
    why we allow `DataCube` as "data" parameter type.
    """

    def process(self, arguments):
        data = self.validate_parameter(arguments, "x", required=True, allowed_types=[float, type(None)])
        inputMin = self.validate_parameter(arguments, "inputMin", required=True, allowed_types=[float])
        inputMax = self.validate_parameter(arguments, "inputMax", required=True, allowed_types=[float])
        outputMin = self.validate_parameter(arguments, "outputMin", default=0, allowed_types=[float])
        outputMax = self.validate_parameter(arguments, "outputMax", default=1, allowed_types=[float])

        if data is None:
            return None

        if math.isclose(inputMin, inputMax):
            raise ProcessParameterInvalid(
                "linear_scale_range", "inputMin", "Argument must differ from argument 'inputMax'."
            )

        original_type_was_number, data = self.convert_to_datacube(data)

        if data.size == 0:
            return None

        results = ((data - inputMin) / (inputMax - inputMin)) * (outputMax - outputMin) + outputMin

        if original_type_was_number:
            return float(results)

        if isinstance(results, DataCube):
            results.attrs["simulated_datatype"] = (float,)

        return results
