#
# Copyright (c) Sinergise, 2019 -- 2021.
#
# This file belongs to common component "Sen4CAP component API" of project NIVA (www.niva4cap.eu).
# All rights reserved.
#
# This source code is licensed under the Apache License v2 license found in the LICENSE
# file in the root directory of this source tree.
#

from os.path import dirname, basename, isfile, join
import glob
import importlib

from ._common import (
    ExecFailedError,
    UserError,
    Internal,
    ProcessParameterInvalid,
    VariableValueMissing,
    ProcessUnsupported,
    StorageFailure,
)

""" Find all.py files in this directory that do not start with underscore, and
    make them a part of this package.
"""
modules_files = glob.glob(join(dirname(__file__), "*.py"))
modules = [basename(f)[:-3] for f in modules_files if isfile(f) and not basename(f).startswith("_")]
for f in modules:
    importlib.import_module(".{process_id}".format(process_id=f), package=__package__)
